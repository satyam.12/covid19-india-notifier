'''
COVID-19 notifier that displays notification as per timer set
'''
from plyer import notification
from configparser import ConfigParser
from apscheduler.schedulers.blocking import BlockingScheduler
import requests
import json

# configurations
config = ConfigParser()
config.read('config.ini')
scheduler = BlockingScheduler()

# function to  show notification


def show_status(head, content):
    notification.notify(
        title=head,
        message=content,
        timeout=int(config['Timer']['msg_duration']),
        app_icon=config['Source']['icon_path']
    )

# function to get the data from URL


def extract_url_data(url):
    data = requests.get(url).json()
    return data

# function to extract URL response and transforms it then sends it to
# show_status function


def data_etl(raw_data):
    state_list = config['States']['list']
    nation = config['States']['nation'].lower()
    if nation == 'on':
        title = 'Status of COVID-19 INDIA'
        content = f'INDIA: {raw_data["statewise"][0]["state"]} Cases'
        content += f'\nConfirmed: {raw_data["statewise"][0]["confirmed"]}'
        content += f'\nActive: {raw_data["statewise"][0]["active"]}'
        content += f'\nRecovered: {raw_data["statewise"][0]["recovered"]}'
        content += f'\nDied: {raw_data["statewise"][0]["deaths"]}'
        show_status(head=title, content=content)
    for data in raw_data['statewise']:
        if data['state'] in state_list:
            title = 'Status of COVID-19 INDIA'
            content = f'State: {data["state"]}\n'
            content += f'Confirmed: {data["confirmed"]}\n'
            content += f'Active: {data["active"]}\n'
            content += f'Recovered: {data["recovered"]}\n'
            content += f'Died: {data["deaths"]}'
            show_status(head=title, content=content)


# function to perform all the required processing
def processing():
    return data_etl(extract_url_data(config['Source']['api_url']))


# scheduling the notifier
scheduler.add_job(
    processing,
    'interval',
    hours=int(config['Timer']['notification_timer'])) # instead of hours you can do minutes or seconds
processing()  # calling it to display result for 1st time
scheduler.start()
